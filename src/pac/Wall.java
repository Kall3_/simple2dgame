package pac;

import java.awt.*;

public class Wall extends Entity
{
    int width;
    int height;
    Boolean floor;

    public Wall(int x, int y, int width, int height, Boolean floor)
    {
        posX = x;
        posY = y;
        this.width = width;
        this.height = height;
        this.floor = floor;
    }

    @Override
    public void draw(Graphics bbg)
    {
        bbg.setColor(Color.BLACK);
        bbg.fillRect(posX, posY, width, height);
    }

    @Override
    Rectangle getBounds() {
        return new Rectangle(posX, posY, width, height);
    }
}
