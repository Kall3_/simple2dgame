package pac;

import java.awt.*;

abstract class MovingEntity extends Entity{
    int oldPosX;
    int oldPosY;
    int velocityUp;
    boolean inAir;

    final int gravity = 1;

    abstract void move(int x, int y);

    abstract void fall();
}
