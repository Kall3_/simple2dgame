package pac;

import java.awt.*;

public class Player extends MovingEntity
{
    int size;
    boolean inAir = true;

    public Player(int x, int y, int size)
    {
        oldPosX = x;
        posX = x;
        oldPosY = y;
        posY = y;
        this.size = size;
        velocityUp = 0;
    }

    public void draw(Graphics g) {
        g.setColor(Color.WHITE);
        g.fillRect(oldPosX, oldPosY, size, size);
        g.setColor(Color.BLACK);
        g.fillRect(posX, posY, size, size);
    }

    @Override
    Rectangle getBounds() {
        return new Rectangle(posX, posY, size, size);
    }

    Rectangle setBounds(int x, int y) { return new Rectangle(x, y, size, size); }

    @Override
    public void move(int posX, int posY) {
        oldPosX = this.posX;
        oldPosY = this.posY;
        this.posX = this.posX + posX;
        this.posY = this.posY + posY;
    }

    @Override
    void fall() {
        move(0, velocityUp - gravity);
    }

    int getSize() {
        return size;
    }
}
