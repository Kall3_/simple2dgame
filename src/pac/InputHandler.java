package pac;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.HashSet;


public class InputHandler implements KeyListener
{
    private HashSet<Integer> keysPressed;

    public InputHandler(Game game)
    {
        keysPressed = new HashSet();
        game.addKeyListener(this);
    }

    @Override
    public void keyPressed(KeyEvent e)
    {
        keysPressed.add(e.getKeyCode());
    }

    @Override
    public void keyReleased(KeyEvent e) { keysPressed.remove(e.getKeyCode()); }

    @Override
    public void keyTyped(KeyEvent e) { }

    public HashSet getKeysPressed() { return keysPressed; }
}