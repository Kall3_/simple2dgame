package pac;

import java.awt.*;

public class Obstacle extends Entity
{
    int size;

    public Obstacle(int posX, int posY, int size)
    {
        this.posX = posX;
        this.posY = posY;
        this.size = size;
    }

    public void draw(Graphics bbg)
    {
        bbg.setColor(Color.RED);
        bbg.fillRect(posX, posY, size, size);
    }

    @Override
    Rectangle getBounds() {
        return new Rectangle(posX, posY, size, size);
    }
}
