package pac;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashSet;

public class Game extends JFrame
{
    boolean isRunning = true;
    int fps = 60;
    int windowWidth = 16*50;
    int windowHeight = 9*50;
    int perSecond;
    int wallSize;
    private boolean doNotAccelerate = false;

    Player p1;
    Obstacle o1;
    ArrayList<MovingEntity> moving;
    ArrayList<Wall> walls;

    BufferedImage backBuffer;
    Insets insets;
    InputHandler input;

    public static void main(String[] args)
    {
        Game game = new Game();
        game.run();
        System.exit(0);
    }

    public void run()
    {
        initialize();

        while(isRunning)
        {
            long time = System.currentTimeMillis();

            update();
            draw();

            time = (1000 / fps) - (System.currentTimeMillis() - time);

            if (time > 0)
            {
                try
                {
                    Thread.sleep(time);
                }
                catch(Exception e){}
            }
        }

        try
        {
            Thread.sleep(1000);
        }
        catch (InterruptedException e) {}

        setVisible(false);
    }

    private void initialize()
    {
        setTitle("Game");
        setSize(windowWidth, windowHeight);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);

        insets = getInsets();
        setSize(insets.left + windowWidth + insets.right,
                insets.top + windowHeight + insets.bottom);

        backBuffer = new BufferedImage(windowWidth, windowHeight, BufferedImage.TYPE_INT_RGB);
        input = new InputHandler(this);

        o1 = new Obstacle(100, 100, 15);
        walls = new ArrayList<>();
        wallSize = 10;
        walls.add(new Wall(0,0, windowWidth, wallSize, false));
        walls.add(new Wall(0, 0, wallSize, windowHeight, false));
        walls.add(new Wall(windowWidth-wallSize, 0, wallSize, windowHeight, false));
        walls.add(new Wall(0, windowHeight-wallSize, windowWidth, wallSize, true));

        moving = new ArrayList<>();
        p1 = new Player(10, 10, 20);
        moving.add(p1);
    }

    private void update()
    {
        moveEntities();
        collision();
    }

    private void collision() {
        Rectangle p1Bounds = p1.getBounds();
        Rectangle o1Bounds = o1.getBounds();
        System.out.println(p1.getBounds().getY());
        if(p1Bounds.intersects(o1Bounds))
        {
            isRunning = false;
        }
    }

    private void moveEntities() {
        HashSet<Integer> keys = input.getKeysPressed();
        for(MovingEntity e : moving)
        {
            if(e instanceof Player)
            {
                if(keys != null && !keys.isEmpty())
                {
                    for(int key : keys)
                    {
                        if(KeyEvent.VK_ESCAPE == key)
                        {
                            isRunning = false;
                        }
                        if (KeyEvent.VK_LEFT == key)
                        {
                            tryMove(e,-4, 0);
                        }
                        if (KeyEvent.VK_RIGHT == key)
                        {
                            tryMove(e,4, 0);
                        }
                        if (KeyEvent.VK_UP == key && !e.inAir)
                        {
                            System.out.println("Jump!");
                            e.inAir = true;
                            e.velocityUp = 30;
                            tryMove(e,0, 0);
                        }
                    }
                }
            }
            if(e.velocityUp > -10)
            {
                e.velocityUp -= e.gravity;
            }
            tryMove(e, 0, -e.velocityUp);
        }
    }

    private void tryMove(MovingEntity e, int x, int y)
    {
        e.move(x, y);
        for(Wall w : walls)
        {
            if(e.getBounds().intersects(w.getBounds()))
            {
                if (w.floor && (e.posY == 440-20))
                {
                    System.out.println("HERE");
                    e.velocityUp = 0;
                    e.inAir = false;
                    return;
                }
                else if (w.floor) {
                    e.velocityUp = 0;
                    e.inAir = false;
                    e.move(0, windowHeight - e.posY - 20 - w.height);
                    System.out.println("here: " + (windowHeight - e.posY - 20 - w.height));
                    return;
                }
                e.move(-x, -y);
                return;
            }
        }
        if(true)
        {
            System.out.println(e.velocityUp);
            if (e.velocityUp > -10)
            {
                e.velocityUp -= e.gravity;
            }
            System.out.println(e.velocityUp);
        }
    }

    private void draw()
    {
        Graphics g = getGraphics();
        Graphics bbg = backBuffer.getGraphics();

        bbg.setColor(Color.WHITE);
        bbg.fillRect(0, 0, windowWidth, windowHeight);

        p1.draw(bbg);
        o1.draw(bbg);
        for(Wall w : walls) { w.draw(bbg); }

        if(!isRunning)
        {
            drawGameOver(bbg);
        }

        g.drawImage(backBuffer, insets.left, insets.top, this);
    }

    private void drawGameOver(Graphics bbg) {
        String text = "Game over!";
        Font font = new Font("TimesRoman", Font.PLAIN, 25);
        Rectangle rect = new Rectangle(windowWidth, windowHeight);
        FontMetrics metrics = bbg.getFontMetrics(font);

        int x = rect.x + (rect.width - metrics.stringWidth(text)) / 2;
        int y = rect.y + ((rect.height - metrics.getHeight()) / 2) + metrics.getAscent();
        bbg.setFont(font);
        bbg.drawString(text, x, y);
    }
}
