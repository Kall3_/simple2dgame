package pac;

import java.awt.Graphics;
import java.awt.Rectangle;

abstract class Entity {
    int posX;
    int posY;

    abstract void draw(Graphics bbg);

    abstract Rectangle getBounds();
}
